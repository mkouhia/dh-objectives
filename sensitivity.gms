$include Regional.gms

* Sensitivity analyses
Sets
    i_obj           Objective function index / 1*5 /
    sen_i           Index of sensitivity run (upper limit)  /1*1000/
    nn(sen_i)       Incremental counter: index of sensitivity run
    steps           Number of steps in sensitivity analyses / st1*st21 /
    solveAtt        Solve attributes /solveStat, modelStat/
    sen_comm(COMM)  Sensitivity to commodity prices
        / ELEC, HDH, BIO, NG /
;

Parameters
*   Input parameters
    wt_opt(steps)        Numeric weight values for factor levels

*   Intermediate values
    save_price(COMM, TRADE)     Save original price table

*   Results
    sen_stat(sen_i, solveAtt)   Solve statistics
    sen_wts(sen_i, sen_comm)    Parameter values
    sen_obj(sen_i, i_obj)       Objective values

*   Stored values from original model
    sen_z_units(sen_i, UNITS)    Unit existence
    sen_P_DH(sen_i, UNITS)       Unit maximum DH output in MW
;

Scalar variance  Variance in inputs in one direction: factor 0..1 / 0.5 /;

parameter sen_report(*, *, *, *) Sensitivity report;


* Calculate weight factor levels based on their number
wt_opt(steps) = (1 - variance)
         + 2 * variance * (ord(steps) - 1) / (card(steps) - 1);


* Solve effect of commodity prices to case 1 (max.profit)
wt(CASES) = 0;
wt('C1') = 1;

nn('1') = yes;
save_price(COMM, TRADE) = price(COMM, TRADE);

loop(sen_comm,
    price(COMM, TRADE) = save_price(COMM, TRADE);
    loop(steps,
        price(COMM, TRADE) = save_price(COMM, TRADE);
        price(sen_comm, TRADE) = price(sen_comm, TRADE) * wt_opt(steps);
        SOLVE REGION1 using MIP minimizing system_cost;

*       Record results
        sen_wts(nn, sen_comm) = wt_opt(steps);
        sen_obj(nn, '1') = -profit.l;
        sen_obj(nn, '2') = ex_loss_t.l;
        sen_obj(nn, '3') = fossil_CO2.l;
        sen_obj(nn, '4') = pef_sys.l;
        sen_obj(nn, '5') = pexa_dh_sys.l;
        sen_stat(nn, 'solveStat') = REGION1.solveStat;
        sen_stat(nn, 'modelStat') = REGION1.modelStat;
        sen_z_units(nn, UNITS) = z_units.l(UNITS);
        sen_P_DH(nn, UNITS) = smax(PERIODS, P_DH.l(UNITS, PERIODS));

        sen_report(sen_comm, 'profit', 'value', steps) = profit.l;
        sen_report(sen_comm, sen_comm, 'factor', steps) = wt_opt(steps);
        sen_report(sen_comm, UNIT_FX, 'exist', steps) = 1$z_units.l(UNIT_FX);
        sen_report(sen_comm, UNIT_SC, 'P_DH', steps) =
            sc_unit_size_dh.l(UNIT_SC)$z_units.l(UNIT_SC);

*       Increment counter for the next loop
        nn(sen_i) = nn(sen_i-1);

    );
);
price(COMM, TRADE) = save_price(COMM, TRADE);

display wt_opt;
display sen_report;

execute_unload "sensitivity.gdx" ,
    i_obj, sen_i, steps, solveAtt, sen_comm
    wt_opt, sen_stat, sen_wts, sen_obj, sen_z_units, sen_P_DH
;
