$include Regional.gms

Parameters
    load_avg(UNITS)    Average annual loading rate [0..1]
    pri_e_tot          "Total primary energy consumption with power bonus [GWh/a]"
    pri_ex_tot         "Total primary exergy consumption with power bonus [GWh/a]"
;

* Run all the scenarios
parameter report(*, *, *)  Solution report (base case);
parameter p_out_cases(UNITS, OUTP, PERIODS, CASES) Power outputs in each simulation case;

loop(CASES,
    wt(CASES-1) = 0;
    wt(CASES) = 1;

*   Reset initial values for case simulation
    sc_unit_size_dh.l(UNIT_SC) = smax(PERIODS, heat_demand(PERIODS)) * 0.3;
    z_units.l(UNITS) = 0;
    z_unit_on.l(UNITS, PERIODS) = 0;
    P_in.l( FEED, UNITS, PERIODS) = 0;
    P_out.l(OUTP, UNITS, PERIODS) = 0;

    SOLVE REGION1 using MIP minimizing system_cost;

*   Objective function values
    report('profit', 'value', CASES) = profit.l;
    report('ex_loss_t', 'value', CASES) = ex_loss_t.l;
    report('fossil_CO2', 'value', CASES) = fossil_CO2.l;
    report('pef_sys', 'value', CASES) = pef_sys.l;
    report('pexa_dh_sys', 'value', CASES) = pexa_dh_sys.l;

*   Levelized cost of energy and DH price with power bonus method
    lcoe = sum((UNITS, MONEY_ITEMS), cost.l(UNITS, MONEY_ITEMS)) * 1e6 / sum(
        (UNITS, OUTP), energy_out.l(UNITS, OUTP));
    dhp_pb = (sum((UNITS, MONEY_ITEMS), cost.l(UNITS, MONEY_ITEMS))
        - sum((UNITS, OUTP)$(not DHGRID(OUTP)),
            income.l(UNITS, OUTP))) * 1e6 / sum(
        UNITS, energy_out.l(UNITS, 'HDH'));
    report('lcoe', 'value', CASES) = lcoe;
    report('dhp_pb', 'value', CASES) = dhp_pb;

* Primary energy/exergy consumption [GWh/a]
    pri_e_tot  = sum((UNITS, PERIODS),
        pri_e.l(UNITS, PERIODS)  * time(PERIODS)) * 1e-3;
    pri_ex_tot = sum((UNITS, PERIODS),
        pri_ex.l(UNITS, PERIODS) * time(PERIODS)) * 1e-3;
    report('pri_e_tot', 'value', CASES) = pri_e_tot;
    report('pri_ex_tot', 'value', CASES) = pri_ex_tot;

*   Plants: built power and utilization rate
    load(UNIT_FX, OUTP, PERIODS)
        = (P_out.l(OUTP, UNIT_FX, PERIODS) / max_output(UNIT_FX, OUTP)
        )$max_output(UNIT_FX, OUTP);
    load(UNIT_SC, 'HDH', PERIODS)
        = (P_DH.l(UNIT_SC, PERIODS) / sc_unit_size_dh.l(UNIT_SC)
        )$(map_out(UNIT_SC, 'HDH') and sc_unit_size_dh.l(UNIT_SC));
    load_avg(UNITS) = sum((OUTP, PERIODS)$map_out(UNITS, OUTP),
        load(UNITS, OUTP, PERIODS) * time(PERIODS))
        / (8760 * sum(OUTP$map_out(UNITS, OUTP), 1));
    report('load_avg', UNITS, CASES) = load_AVG(UNITS);
    report('P_DH', UNIT_SC, CASES) = sc_unit_size_dh.l(UNIT_SC)$(
        map_out(UNIT_SC, 'HDH') and
        sum(PERIODS, P_DH.l(UNIT_SC, PERIODS)) > 0);

    p_out_cases(UNITS, OUTP, PERIODS, CASES) = P_out.l(OUTP, UNITS, PERIODS);

);

display report, p_out_cases;
execute_unload "scenarios.gdx" , report, p_out_cases, time;
