$include Regional.gms
$eolcom //


Sets
    i_obj      Objective function index / 1*5 /
    i_cp       Index for CP - Amount of weight combinations (upper limit) /1*1000/
    i_im(i_cp) Index for independent minimization / 1*5 /
    nn(i_cp)   Incremental counter: index of weight factor combination
    steps      Weighing factor levels for evaluation /st1*st11/

    stat       Statistics /min, max/
    solveAtt   Solve attributes /solveStat, modelStat/
;


Parameters
*   Input parameters
    im_wts(i_im, i_obj)  Weights for independent minimization
    cp_wts(i_cp, i_obj)  Weights for compromise programming
    wt_opt(steps)        Numeric weight values for factor levels

    wt_f(i_obj)          Objective function weighing factors for each run

*   Intermediate results
    f_lim(i_obj, stat)   Extremes for each objective function

*   Results
    im_stat(i_im, solveAtt) Independent minimization statistics
    im_obj(i_im, i_obj)     Objective values for independent minimization
    cp_stat(i_cp, solveAtt) CP statistics
    cp_obj(i_cp, i_obj)     Objective values
    cp_dist(i_cp)           Chebyshev distance values

*   Stored values from original model
    cp_z_units(i_cp, UNITS) Unit existence
    cp_P_DH(i_cp, UNITS)    Unit maximum DH output in MW

*   Derivative results
    cp_m(i_cp, i_obj)       Objective function fuzzy membership or `objective success percent`
;

Scalar cp_sol_n Number of CP solution points (counter) /0/;


Variables
    objective(i_obj) Objective function vector
    objsum           Objective level
    dist             Chebyshev distance for compromise programming
;


Equations
    o1, o2, o3, o4, o5 Combine Regional.gms objectives into one objective vector
    o_tot              Sum of objective functions for independent minimization
;


o1.. objective('1') =e= -profit;
o2.. objective('2') =e= ex_loss_t;
o3.. objective('3') =e= fossil_CO2;
o4.. objective('4') =e= pef_sys;
o5.. objective('5') =e= pexa_dh_sys;

o_tot.. objsum =e= sum(i_obj, objective(i_obj) * wt_f(i_obj));


Option limrow   = 0 ;
Option limcol   = 0 ;
Option solprint = off ;
Option sysout   = off ;

file con; // Outputs to console


put con; put 'Generating all pareto-optimal solutions'; putclose;



* Step 1: minimize objectives independently
put con; put / '# Minimizing objectives independently'; putclose;

* Weighing factors for independent minimization (identity matrix)
loop(i_im$(ord(i_im) le card(i_obj)),
    im_wts(i_im, i_obj) = 1$(ord(i_im) eq ord(i_obj)) );


Model min_all 'Independent minimization' / all / ;

loop(i_im$(ord(i_im) le card(i_obj)),
    wt_f(i_obj) = im_wts(i_im, i_obj); // Select weigts for this run

    Solve min_all using MIP minimizing objsum;

*   Record results
    im_obj(i_im, i_obj) = objective.l(i_obj);
    im_stat(i_im, 'solveStat') = min_all.solveStat;
    im_stat(i_im, 'modelStat') = min_all.modelStat;
);

display im_wts, im_obj, im_stat;


* Step 2: find minimum and maximum values for individual objectives
put con; put 'Recording minimum and maximum objective function values'; putclose;

f_lim(i_obj, 'min') = smin(i_im, im_obj(i_im, i_obj));
f_lim(i_obj, 'max') = smax(i_im, im_obj(i_im, i_obj));

display f_lim;


* Step 3: Generate weight combinations for compromise programming

put con;
put / '# Compromise programming' / ;
put / '## Weighing factors' / ;
put 'Generating weighing factor combinations for simulation...';
putclose;

* Calculate weight factor levels based on their number
wt_opt(steps) = (ord(steps) - 1) * (1 / (card(steps) - 1));


alias(steps, s1,s2,s3,s4,s5); // Individual factor sets for each objective function
nn('1') = yes;
loop((s1,s2,s3,s4,s5),
*   Find all combinations of weight factors, where their sum equals unity
    if ((wt_opt(s1) + wt_opt(s2) + wt_opt(s3) + wt_opt(s4) + wt_opt(s5) eq 1),

*       Echo intermediate status
        cp_sol_n = cp_sol_n + 1;
        if ((mod(cp_sol_n, 100) eq 0), put con; put cp_sol_n::0; putclose;);

*       Save weight factors
        cp_wts(nn, '1') = wt_opt(s1);
        cp_wts(nn, '2') = wt_opt(s2);
        cp_wts(nn, '3') = wt_opt(s3);
        cp_wts(nn, '4') = wt_opt(s4);
        cp_wts(nn, '5') = wt_opt(s5);

*       Increment counter for the next loop
        nn(i_cp) = nn(i_cp-1);

*       Check that the allocated n size is large enough
        if ((cp_sol_n eq card(i_cp)),
            put con; put "!!!! OVERFLOW: n is too small set (";
            put card(i_cp):0:0; put ") -- can't fit all solutions"; putclose;
        )
    );
);
put con; put cp_sol_n:0:0; put ' possible weight combinations found'; putclose

display cp_wts;
display cp_sol_n;



* Step 4: Compromise programming method for pareto front finding

Equation bound_cp(i_obj)     Boundaries for compromise programming;

bound_cp(i_obj)..
    wt_f(i_obj) * (objective(i_obj) - f_lim(i_obj, 'min'))
    / (f_lim(i_obj, 'max') - f_lim(i_obj, 'min'))
    =L= dist;

Model compromise 'Compromise programming' / all / ;


put con;
put / '## Pareto front' / ;
put 'Solving scenarios...'; putclose;

scalar t_start, t_est;
t_start = jnow;

* Loop over set of weighing factors, record results.
*     Also redo analysis for initial points (TODO: ensure that results are the same)
*     Include estimate of remaining runtime
loop(i_cp$(sum(i_obj, cp_wts(i_cp, i_obj))),
*   Echo intermediate status every 10 solves
    if ((mod(ord(i_cp), 10) eq 0),
        put con;
        put 'Solve '; put ord(i_cp):3:0; put ' / '; put cp_sol_n:0:0;
        t_est = (jnow-t_start)/(ord(i_cp)-1) * (cp_sol_n-ord(i_cp)+1) * 24 * 60;
        put ', approx. '; put t_est:0:1; put ' minutes remaining';
        putclose;
    );

*   Replace weighing factors for the objective function
    wt_f(i_obj) = cp_wts(i_cp, i_obj);

    Solve compromise using MIP minimizing dist;

*   Record results
    cp_obj(i_cp, i_obj) = objective.l(i_obj);
    cp_dist(i_cp) = dist.l;
    cp_stat(i_cp, 'solveStat') = compromise.solveStat;
    cp_stat(i_cp, 'modelStat') = compromise.modelStat;

    cp_z_units(i_cp, UNITS) = z_units.l(UNITS);
    cp_P_DH(i_cp, UNITS) = smax(PERIODS, P_DH.l(UNITS, PERIODS));
);

display cp_wts, cp_obj, cp_dist, cp_stat;


* Step 4: Fuzzy method - calculate solution vitality with minmax method
put con;
put / '## Solution vitality' / ;
put 'Calculating solution goodness by fuzzy (minmax) method';
putclose;

cp_m(i_cp, i_obj)$(cp_stat(i_cp, 'solveStat') eq 1)
    = (f_lim(i_obj, 'max') - cp_obj(i_cp, i_obj))
    / (f_lim(i_obj, 'max') - f_lim(i_obj, 'min'));

display cp_m;


execute_unload "cp.gdx" ,
    i_obj, i_cp, i_im, stat, solveAtt,
    im_wts, cp_wts,
    f_lim,
    im_stat, im_obj,
    cp_stat, cp_obj, cp_dist,
    cp_z_units, cp_P_DH,
    cp_m
;
