# Generate power output graphs

if (!require(gdxtools)) {library("devtools"); install_github("lolow/gdxtools")}

library(tidyverse)
library(glue)
library(gdxtools) # Process GAMS GDX file directly with gdxtools

# Setup gams connection
igdx() # If this does not work, either set environment variable R_GAMS_SYSDIR in $HOME/.Renviron or use parameter gamsSysDir

scen <- gdx("scenarios.gdx") # Straight out of GAMS21.3: column names are missing.

p_out <- as.tibble(scen["p_out_cases"]) %>%
  transmute(
    Unit = as.factor(V1),
    Output = as.factor(V2),
    Period = as.factor(V3),
    Case = as.factor(V4),
    Power = value) %>%
  # Rename parametres for article output
  mutate(Output = recode(Output, HDH = "DH")) %>%
  mutate(Unit = recode(Unit,
    BHOB1 = "bioHOB",
    BIOSNG1 = "SNG",
    BIOCHP1 = "bioCHP",
    HP1 = "HP",
    GHOB1 = "gasHOB"))

dur <- as.tibble(scen["time"]) %>% transmute(Period = V1, value = value)
endtimes <- append(cumsum(dur$value), 0, after=0)
midtimes <- (endtimes[-length(endtimes)] + endtimes[-1] )/2
duration <- dur$value[match(p_out$Period, dur$Period)]
timex <- midtimes[match(p_out$Period, dur$Period)]


unit_colours <- c(
  "gasHOB" = "#4A1A2B",
  "GE1" = "#514D8D",
  "GE2" = "#8582af",
  "GE3" = "#b9b7d1",
  "GE4" = "#ededf3",
  "bioCHP" = "#479C7F",
  "HP" = "#AEB272",
  "bioHOB" = "#F4C4D5",
  "SNG" = "grey"
)

for (c_plot in levels(p_out$Case)){
  df <- p_out[p_out$Case==c_plot,]
  df <- df[,-which(names(df) == "Case")]
  # timex <- timex[which(p_out$Case==c_plot)]
  df$duration <- dur$value[match(df$Period, dur$Period)]
  df$tx <- midtimes[match(df$Period, dur$Period)]

  for (op in unique(df$Output)){
    df2 <- df[df$Output == op,]
    p <-ggplot(df2, aes(x=tx, y=Power, fill=Unit)) +
      geom_bar(stat="identity", width=df2$duration) +
      xlim(0, 8760) +
      theme_bw() +
      theme(legend.justification=c(1,1), legend.position=c(0.99,0.99),
            legend.direction = "horizontal",
            legend.key.size=unit(4, "mm"),
            legend.margin=margin(4,4,4,4),
            legend.background=element_rect(color="gray30", size=0.2)) +
      theme(text=element_text(size=8), title=element_text(size=8)) +
      scale_fill_manual(values=unit_colours) +
      # scale_x_continuous(limits=c(0, 8760), expand = c(0, 0)) +
      labs(x="Time/h", y="Power/MW")

    ggsave(sprintf("out/output_%s-%s.pdf", c_plot, op), width=90, height=35, units="mm")
    # ggsave(sprintf("out/output_%s-%s.png", c_plot, op), width=90, height=35, units="mm", dpi=600)
  }
}
