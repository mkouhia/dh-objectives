source("process/pareto.r")
library(GGally) # matrix of plots
library(plotly)


# Get only lower part of ggpairs https://stackoverflow.com/a/42656454
gpairs_lower <- function(g){
  g$plots <- g$plots[-(1:g$nrow)]
  g$yAxisLabels <- g$yAxisLabels[-1]
  g$nrow <- g$nrow -1

  g$plots <- g$plots[-(seq(g$ncol, length(g$plots), by = g$ncol))]
  g$xAxisLabels <- g$xAxisLabels[-g$ncol]
  g$ncol <- g$ncol - 1

  g
}

shapes <- rep(c(15,0,16,1,17,2,18,5), 5)
n_combo <- n_distinct(res$unit_combo)
ch_colors <- rje::cubeHelix(n = (n_combo + 2), hue = 1.4 )[1:n_combo]

#points_legend <- gglegend(ggally_points)
res_plot <- select(res, 'f[1]' = f_1, 'f[2]' = f_2, 'f[3]' = f_3,
                   'f[4]' = f_4, 'f[5]' = f_5, unit_combo)
g <- ggpairs(
  res_plot, columns = 1:(ncol(res_plot)-1),
  labeller = "label_parsed",
  diag = NULL, upper = NULL,
  legend = c(2,1),
  mapping = aes(colour=unit_combo, shape=unit_combo)
) +
  theme_bw() +
  theme(legend.key.size=unit(4, "mm")) +
  theme(text=element_text(size=8), title=element_text(size=8)) +
  theme(legend.position = "bottom") +
  theme(legend.title=element_blank())
pairs <- list(c(1,2), c(1,3), c(1,4), c(1,5), c(2,3), c(2,4), c(2,5), c(3,4), c(3,5), c(4,5))
for (row in seq_len(g$nrow)) {
  for (col in seq_len(g$ncol)) {
    g[row, col] <- g[row, col] + scale_color_manual(values=ch_colors) + scale_shape_manual(values=shapes)
    for (pair in pairs) {
      if (pair[1] == col & pair[2] == row) {
        gt <- g[row, col] + labs(x = bquote(f[.(row)]), y = bquote(f[.(col)]))
        ggsave(sprintf("out/plot_f%d-f%d.png", row, col), gt, width=180, height=150, units="mm")
      }
    }
  }
}
f <- gpairs_lower(g)
#ggsave("out/moo-matrix.png", f, width=180, height=150, units="mm", dpi=600)
ggsave("out/moo-matrix.pdf", f, width=180, height=150, units="mm", dpi=600)

# Show only points where f_2 and f_5 get no weights
rf <- res %>% filter(w_2==0 & w_5==0)
g <- ggpairs(
  rf, columns = c(2,4,5),
  labeller = "label_parsed",
  diag = NULL, upper = NULL,
  legend = c(2,1),
  mapping = aes(colour=unit_combo, shape=unit_combo)
) +
  theme_bw() +
  theme(legend.key.size=unit(4, "mm")) +
  theme(text=element_text(size=8), title=element_text(size=8)) +
  theme(legend.position = "bottom") +
  theme(legend.title=element_blank())
for (row in seq_len(g$nrow))
  for (col in seq_len(g$ncol))
    g[row, col] <- g[row, col] + scale_color_manual(values=ch_colors) + scale_shape_manual(values=shapes)
f <- gpairs_lower(g)
#ggsave("out/moo-matrix3.png", f, width=180, height=150, units="mm", dpi=600)


# Export plot_ly figures to Internet for publication
for (pair in list(c(1,2), c(1,3), c(1,4), c(1,5), c(2,3), c(2,4), c(2,5), c(3,4), c(3,5), c(4,5))){
  f_x <- pair[1]
  f_y <- pair[2]
  print(sprintf("Generating Plot.ly html images: f%d~f%d", f_x, f_y))
  xlab <- list(title=sprintf("f<sub>%d</sub>", f_x))
  ylab <- list(title=sprintf("f<sub>%d</sub>", f_y))
  p <- plot_ly(
    res, x = pull(res[,1+f_x]), y = pull(res[,1+f_y]), # objective values are in columns 2:6
    type = "scatter", mode = "markers",
    color = ~unit_combo, colors=ch_colors,
    symbol = ~unit_combo,
    symbols = shapes,
    text = ~paste("Power (MW): ", powers),
    hoverinfo = "text") %>%
    layout(xaxis=xlab, yaxis = ylab)
  fn <- file.path(normalizePath("out"), sprintf("plot_f%d-f%d.html", f_x, f_y))
  ln <- file.path(normalizePath("out"), "plot_files")
  htmlwidgets::saveWidget(p, fn, selfcontained=FALSE, libdir=ln)
}


# Units vs objectives
t1 <- tmp %>% gather(UNITS, value, -i_cp, na.rm=TRUE) %>% arrange(i_cp) %>%
  group_by(i_cp) %>%
  filter(value != FALSE)
t2 <- cp_obj %>% rename(f_i = i_obj)
r3 <- full_join(t1, t2, by="i_cp")
r3 %>% ungroup() %>% ggplot(aes(x=UNITS, y=value.y)) + geom_point(alpha=0.2) + facet_grid(f_i ~ ., scales="free_y")

# Unit combos vs objectives
full_join(u2, t2, by="i_cp") %>% ggplot(aes(x=unit_combo, y=value)) + geom_point(alpha=0.2) + facet_grid(f_i ~ ., scales="free_y") + theme(axis.text.x = element_text(angle = 30, hjust = 1))


res %>%
  filter(w_1 > 0 & w_5 == 0 & w_2 == 0) %>%
  plot_ly(x = ~f_1, y = ~f_4,
          type = "scatter", mode = "markers",
          symbols = shapes,
          color = ~desc(f_3),
          text = ~paste("i: ", i_cp,
                        "<br />Power (MW): ", powers,
                        "<br />w1:w2:w3:w4:w5 ", w_1, w_2, w_3, w_4, w_5),
          hoverinfo = "text")

# Avoided CO2 cost
p <- ggplot(res_cost, aes(y=cost_avoided, x=df3)) +
  geom_point() +
  theme_bw() +
  theme(text=element_text(size=8), title=element_text(size=8)) +
  ylim(0, NA) +
  labs(x = expression(paste("Removed ", "CO"[2], " (1000 t/a)")),
       y = expression(paste("Removed ", "CO"[2], " cost (EUR/t)")))
ggsave("out/co2-cost.pdf", p, width=90, height = 60, units="mm")
