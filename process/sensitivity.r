if (!require(gdxtools)) {library("devtools"); install_github("lolow/gdxtools")}

library(tidyverse)
library(glue)
library(gdxtools) # Process GAMS GDX file directly with gdxtools

# Setup gams connection
igdx() # If this does not work, either set environment variable R_GAMS_SYSDIR in $HOME/.Renviron or use parameter gamsSysDir

cp <- gdx("sensitivity.gdx") # Straight out of GAMS21.3: column names are missing.


sen_wts <- as.tibble(cp['sen_wts']) %>%
  rename(sen_i = V1, sen_comm = V2, wt_opt = value) %>%
  mutate_at(vars("sen_i"), as.integer) %>%
  mutate_at(vars("sen_comm"), as.factor) %>%
  mutate(sen_comm = recode_factor(sen_comm, "ELEC"="ELEC", "NG"="NG", "BIO"="BIO", "HDH"="DH"))


sen_obj <- as.tibble(cp['sen_obj']) %>%
  rename(sen_i = V1, i_obj = V2) %>%
  mutate_at(vars("sen_i"), as.integer)%>%
  mutate_at(vars("i_obj"), as.factor) %>%
  mutate_at(vars("i_obj"), funs(glue("f_{i_obj}"))) %>%
  spread(i_obj, value) %>%
  mutate(profit = -f_1)

sen_u <- as_tibble(cp["sen_z_units"]) %>%
  rename(sen_i = V1, UNITS = V2) %>%
  mutate_at(vars(sen_i), as.integer) %>%
  mutate_at(vars(UNITS), as.factor) %>%
  mutate_at(vars(value), as.logical)

# Unit combo as strings
sen_u_combo <- sen_u %>%
  group_by(sen_i) %>% summarise_at("UNITS", funs(paste(.,collapse=":"))) %>%
  rename(unit_combo = UNITS) %>%
  mutate_at(vars(unit_combo), as.factor)

sen_res <- full_join(sen_wts, sen_obj, by="sen_i") %>%
  full_join(sen_u_combo, by="sen_i")

sen_res
p <- ggplot(sen_res, aes(x=wt_opt, y=profit, colour=sen_comm)) +
  geom_line() +
  geom_point(aes(shape=unit_combo)) +
  labs(x = "Parameter value (share of original)", y = "Profit (MEUR/a)",
       colour = "Commodity", shape = "Units") +
  theme_bw() +
  theme(legend.position = "top",
        legend.direction = "horizontal", legend.box = "vertical",
        legend.box.just = "left",
        legend.key.size=unit(4, "mm"),
        legend.margin = margin(0,0,0,0),
        legend.spacing = unit(0, "mm")
        ) +
  theme(text=element_text(size=8), title=element_text(size=8)) +
  guides(shape=guide_legend(nrow=2)) +
  scale_colour_manual(values = rje::cubeHelix(n = 6)[1:4])
ggsave("out/sensitivity.pdf", p, width = 90, height = 75, units = "mm")
