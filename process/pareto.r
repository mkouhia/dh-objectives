# Process compromise programming results from GAMS

if (!require(gdxtools)) {library("devtools"); install_github("lolow/gdxtools")}

library(tidyverse)
library(glue)
library(gdxtools) # Process GAMS GDX file directly with gdxtools

# Setup gams connection
igdx() # If this does not work, either set environment variable R_GAMS_SYSDIR in $HOME/.Renviron or use parameter gamsSysDir

cp <- gdx("cp.gdx")

# Read objective function values at each pareto-optimal point
cp_obj <- as_tibble(cp["cp_obj"]) %>%
  rename(i_cp = V1, i_obj = V2) %>%
  mutate_at(vars("i_cp"), as.integer)
obj <- cp_obj %>%
  mutate_at(vars("i_obj"), funs(glue("f_{i_obj}"))) %>%
  spread(i_obj, value)
res <- obj


# Read in weighting factors for these simulation runs
cp_wts <- as_tibble(cp["cp_wts"]) %>%
  rename(i_cp = V1, i_obj = V2) %>%
  mutate_at(vars("i_cp"), as.integer)
wts <- cp_wts %>%
  mutate_at(vars("i_obj"), funs(glue("w_{i_obj}"))) %>%
  spread(i_obj, value, fill=0)

res <- full_join(obj, wts, by="i_cp")


# Add in units to the plots

cp_u <- as_tibble(cp["cp_z_units"]) %>%
  transmute(
    i_cp = as.integer(V1),
    UNITS = as.factor(V2),
    value = as.logical(value))

# Test drive: until I know how to combine items as strings, here's a version with integers
u0 <- cp_u %>%
  mutate_at(vars(value), as.integer) %>%
  spread(UNITS, value, fill=0) %>%
  unite(unit_combo, -i_cp, sep="") %>%
  mutate_at(vars(unit_combo), as.factor)

# Unit combo as strings
u1 <- cp_u %>%
  group_by(i_cp) %>% summarise_at("UNITS", funs(paste(.,collapse=":"))) %>%
  rename(unit_combo = UNITS) %>%
  mutate_at(vars(unit_combo), as.factor)

# Combine GEs in unit combinations: reduce amount of levels
tmp <- cp_u %>% spread(UNITS, value)
tmp$GE <- tmp %>% select(GE1:GE4) %>% transmute(ges=as.logical(rowSums(., na.rm=TRUE))) %>% pull()
tmp <- select(tmp, -c(GE1:GE4))
u2 <- tmp %>% gather(key=UNITS, value=value, -i_cp, na.rm=TRUE) %>%
  group_by(i_cp) %>%
  filter(value != FALSE) %>%
  summarise_at("UNITS", funs(paste(.,collapse=":"))) %>%
  rename(unit_combo = UNITS) %>%
  mutate_at(vars(unit_combo), as.factor)

# Units in wide format
units <- spread(cp_u, UNITS, value, fill = FALSE) %>%
  rename(bioHOB=BHOB1, bioCHP=BIOCHP1, gasHOB=GHOB1, HP=HP1)


res <- full_join(res, units, by="i_cp")
res <- full_join(res, u2, by="i_cp")

cp_P_DH <- as_tibble(cp["cp_P_DH"]) %>%
  transmute(
    i_cp = as.integer(V1),
    UNITS = as.factor(V2),
    power = sprintf("%.1f", value)) %>%
  unite(powers, c("UNITS", "power"), sep=":") %>%
  group_by(i_cp) %>%
  summarise_at("powers", funs(paste(.,collapse=", ")))

res <- full_join(res, cp_P_DH, by="i_cp")

# Number of occurrences
unit_combo_occurrences <-  cp_P_DH %>%
  mutate_at(vars(powers), as.factor) %>%
  group_by(powers) %>%
  summarise(n=n()) %>%
  arrange(desc(n))

# Avoided CO2 cost
res_cost <- res %>% select(f_1, f_3, w_1, w_3) %>% filter(w_1 + w_3 == 1)
res_cost$df1 <- res_cost$f_1 - res_cost$f_1[res_cost$w_1 == 1] #delta -profit / MEUR/a (positive number: cost)
res_cost$df3 <- res_cost$f_3[res_cost$w_1 == 1] - res_cost$f_3 #delta CO2 / 10^6 kg/a (positive number: removed CO2)
res_cost$cost_avoided <- res_cost$df1 / res_cost$df3 * 1e3 # EUR / t = avoided cost
