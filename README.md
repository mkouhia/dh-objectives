# Evaluation of design objectives in district heating system design

This repository contains files for research project for evaluating how different goals in district heat planning affect the networks.
See the article manuscript in *manuscript/regional-concept.tex* for detailed description.


## Organization of code
Optimization and analysis are done with [GAMS] and [R].
GAMS version 21.3 has been used in the analysis.

The code has been organized as follows:

- *Regional.gms* defines the optimization model
- *sensitivity.gms* performs sensitivity analysis to economic parameter variation
- *scenarios.gms* runs optimization for all five objective functions separately
- *cp.gms* performs multicriteria analysis with compromise programming method
- Folder *process* houses scripts to analyze optimization outcomes
	- *output_graph.r* generates output figures for individual optimization of the objective functions
	- *sensitivity.r* generates sensitivity figure
	- *pareto.r* and *pareto-figs.r* generate figures for multicriteria optimization.

Data exchange happens via *.gdx* files.


## Optimization and analysis of results

### Individual model runs

Run file *Regional.gms* in GAMS IDE, or in command line with

	gams Regional.gms

Variables are displayed in the output listing file *Regional.lst*, all GAMS sets, parameters and variables are forwarded to *Regional.gdx*.

### Sensitivity to economic parameter variation

Run *sensitivity.gms* and after that *sensitivity.r*, either in e.g. RStudio or in command line:

	gams sensitivity.gms
	Rscript process/sensitivity.r
	
Output files:
- *out/sensitivity.pdf* sensitivity analysis for price parameters

### Output graphs for individual objective function optimization

Run *scenarios.gms* and process results with *output_graph.r*.

	gams scenarios.gms
	Rscript process/output_graph.r

These graphs are shown in the manuscript, Figure 3.
Table 4 in the manuscript follow outputs from *scenarios.lst*, parameter *report*.

Output files:
- *out/output_C?-{DH,ELEC}.pdf* Heat and electricity generation profiles


### Multiobjective optimization

Compromise programming method is employed, optimization happens via script *cp.gms*.
The results are stored in *cp.gdx*, which is processed by R scripts *pareto.r* and *pareto-figs.r*, of which the first does processing and the latter generates images.
Output files appear in folder *out*.

	gams cp.gms lo=0
	Rscript pareto-figs.r

Output files:
- *moo-matrix.pdf* multiobjective optimization results in pairwise matrix
- *plot_f?-f?.png* individual subplots of the previous matrix in greater size, for article supplementary materials
- *plot_f?-f?.pdf* and *plot_files/* interactive plot.ly figures of the previous
- *co2-cost.pdf* marginal cost of removed CO<sub>2</sub> emission
	- The figure in the article has been made with more data points, cp.gms has been edited to run an optimization between f<sub>1</sub> and f<sub>3</sub> with finer spacing.


## License

The software (GAMS files and R scripts) are licensed under the terms of the MIT license, see [LICENSE.md](LICENSE.md).

The accompanying article manuscript (folder *manuscript* and included files) is copyrighted content. The license does not apply to the manuscript.


## Citations

The manuscript will likely be later available via a publishing institution and citations to it are requested to be referred with the digital object identifier (DOI) of the formal publication.
If you use the code and cite it in a document, please use its DOI [doi:10.5281/zenodo.1256442](http://dx.doi.org/10.5281/zenodo.1256442).



[GAMS]: https://www.gams.com/
[R]: https://www.r-project.org/
