$title Regional energy system model
$eolcom //


Sets

    MONEY_ITEMS    Items for cost calculation
                   / BIO, NG, ELEC, HDH, SNG
                     CO2 "Carbon dioxide"
                     INV "Investment"
                     /
    COMM(MONEY_ITEMS) Commodity
                   / BIO  "Biomass chips, 50 % moisture content"
                     NG   "Natural gas"
                     ELEC "Electricity"
                     HDH  "High-temperature district heat"
                     SNG  "Synthetic natural gas"
                     /
    FEED(COMM)     / ELEC, BIO, NG /
    GAS(COMM)      / NG, SNG /
    FUEL(FEED)     / BIO, NG /
    OUTP(COMM)     / ELEC, HDH, SNG /
    PRODUCT(OUTP)  / SNG /
    BIOM(FUEL)     / BIO /
    DHGRID(OUTP)   District heating
                   / HDH /
    UNITS          / GE1 * GE4,
                     BIOSNG1
                     BIOCHP1,
                     HP1,
                     BHOB1,
                     GHOB1 /
    UNIT_SC(UNITS) Scalable units (max. power determined from simulation)
                   / HP1, BHOB1, GHOB1, BIOCHP1, BIOSNG1 /
    UNIT_FX(UNITS) Fixed size units
                   / GE1 * GE4
                     /
    map_in(UNITS, FEED)
                   / (GE1 * GE4, GHOB1).NG
                     (BIOSNG1, BIOCHP1, BHOB1).BIO
                     (BIOSNG1, HP1).ELEC /
    map_out(UNITS, OUTP)
                   / (GE1 * GE4, BIOCHP1).(ELEC, HDH)
                     BIOSNG1.(SNG, HDH)
                     (HP1, BHOB1, GHOB1).HDH /
    GE(UNIT_FX)    / GE1 * GE4/
    GE_TYPE        / 20V34SG, 16V34SG, 9L34SG /
*   Engines of the same GE_TYPE must be adjacent in set GE
    map_GE(GE, GE_TYPE)
                   / (GE1, GE2).20V34SG
                     GE3.16V34SG
                     GE4.9L34SG
                     /
    BIOSNG(UNIT_SC)  / BIOSNG1 /
    BIOCHP(UNIT_SC)  / BIOCHP1 /
    HEATPUMP(UNIT_SC)/ HP1 /
    BIOHOB(UNIT_SC)  / BHOB1 /
    GASHOB(UNIT_SC)  / GHOB1 /
    PERIODS        / P1 * P7 /
    CASES          Different cases (objective functions)
                   / C1  maximum annual profit
                     C2  minimum exergy losses
                     C3  minimum fossil CO2 emissions
                     C4  minimum DH primary energy factor
                     C5  minimum DH PeXa factor
                     /
;

Sets
    P_COMM Commodity properties
        / lhv    "Lower heating value [MJ/kg]"
          cp     "Specific heat capacity [MJ/(kg*K)]"
          ex-c   "Chemical exergy [MJ/kg]"
          ex-c-f "Chemical exergy factor [MW_exergy/MW_energy]"
          temp   "Temperature [K]"
          press  "Pressure [Pa]"
          mmass  "Molar mass [kg/mol]"
          co2-df "Direct fossil CO2 emission [kg CO2/MWh]"
          co2-dr "Direct renewable CO2 emission [kg CO2/MWh]"
          co2-if "Indirect fossil CO2 emission [kg CO2/MWh]"
          f-fg "Flue gas output per fuel input [kg/kg]"
          /
    p_CO2(P_COMM) CO2 flow types / co2-df, co2-dr, co2-if /
    p_CO2_f(p_CO2) Fossil CO2 / co2-df, co2-if /
    P_UNIT Unit properties
        / eff-net-max "Maximum net efficiency [-]"
          eff-el-max  "Maximum electric efficiency [-]"
          max-el      "Maximum electricity production [MW]"
          /
    P_FG(P_COMM) Flue gas properties
        / cp, ex-c, temp, press, mmass, f-fg /
    TRADE   Direction of trading / buy, sell /
;

Parameters
*   Note that different values (cost1, cost2, cost3) are not commensurable
    wt(CASES)        Weighing factors for objective function
        / C1=1, C2=0, C3=0, C4=0, C5=0 /

    time(PERIODS)    Hours in each period [h]
        / P1=12, P2=424, P3=1658, P4=2755, P5=1089, P6=2480, P7=342 /
    cp               Specific heat capacity [MJ*(kg*K)-1]
        / water=4.19e-3 /

    t_in(PERIODS)     DH grid return water (process inlet) temp [K]
    t_out(PERIODS)    DH grid feed water (process outlet) temp [K]

    pef(COMM)        Primary energy factor
        / ELEC=2.5 // European average for marginal production. [ecoheatcool2006dh, iinas2015pef]
          BIO=1.1  // [seai2015tper, Modified from ecoheatcool2006dh]
          NG=1.1   // [ecoheatcool2006dh, seai2015tper]
          SNG=1.1
          /
*   Add/uncomment rows to enable minimum load limitation
    min_load(UNITS, OUTP)  Minimum output (0..1)
        /
          (GE1*GE4).ELEC = 0.1
*         BIOSNG1.SNG = 0.7 // TODO check value
          BIOCHP1.HDH = 0.4           // Savola & Keppo (2005)
          BHOB1.HDH = 0.25            // [energistyrelsen2012data]
          GHOB1.HDH = 0
          /

* Investment costs
* Gas engines: regression model from [EPA2015chp-catalog] data
*   c_GE = (-0.3490 * ln(P_max/MW) + 2.323) USD/MW * 0.8 EUR/USD
* |power       |sp.price    |price       |
* |MW          |MEUR/MW     |MEUR        |
* |------------|------------|------------|
* |8.73        |1.25        |10.94       |
* |6.97        |1.31        |9.17        |
* |3.888       |1.47        |5.75        |
* Bio-SNG plants: 4.1 MEUR/MW(SNG) [sandvall2015modelling] * 5.674 MW(SNG)/MW(th)
* Bio-CHP plants: 4.25 MEUR/MW(el) * (0.25/(1.03-0.25)) power-to-heat ratio -> 1.362 MEUR/MWth [energistyrelsen2012data]
* Heat pumps: 0.52..0.84 MEUR/MW [energistyrelsen2012data]
    inv_cost_fx(UNITS)  Unit investment cost [MEUR]
        / GE1=10.94, GE2=10.94, GE3=9.17, GE4=5.75 /
    s_inv_DH(UNITS)  Specific investment cost per DH output [MEUR*MW-1]
         / BHOB1 = 0.80         // [energistyrelsen2012data]
           GHOB1 = 0.10         // [energistyrelsen2012data]
           BIOCHP1 = 1.362      // [energistyrelsen2012data]
           HP1 = 0.68           // [energistyrelsen2012data]
           BIOSNG1=23.3         // [sandvall2015modelling]
           /

    bigM(UNITS)      Big M

;

t_in(PERIODS) =  50 + 273.15;
t_out(PERIODS) = 100 + 273.15;


Parameter heat_demand(PERIODS)   Heat load in DH grid [MW] /
    P1  17.82
    P2  14.10
    P3  11.42
    P4   8.07
    P5   4.79
    P6   2.68
    P7   1.59
/
;

// TODO add elec transmission+tax to cost calculation separately, not here
Table price(COMM, TRADE) "Energy prices in / out [EUR/MWh]"
            buy    sell
BIO          20
NG           25
ELEC         30      30
HDH                  45
SNG                  25
;
* ## Prices
* ### Natural gas
* - Energy in short term projection approx 25 EUR/MWh, now approx 22 [kaasutilasto]
* - Transmission projection 12 EUR/MWh, now approx 11 [kaasutilasto]
* - Taxes: complicated case. Tax in Finland lies between 6.2 and 19.7 EUR/MWh for the different uses in here. We will use 13 here.
*     - https://www.vero.fi/syventavat-vero-ohjeet/ohje-hakusivu/56228/energiaverotusohje_201/
*     - https://www.vero.fi/yritykset-ja-yhteisot/tietoa-yritysverotuksesta/valmisteverotus/valmisteverolajit/sahko_ja_eraat_polttoaineet/sahkon_ja_eraiden_polttoaineiden_verota/
*
* ### Biomass
* - https://www.baltpool.eu/en/supply-price/?mwh , http://www.foex.fi/biomass/

Parameter other_cost(COMM) "Additional costs [EUR/MWh] -> transmission and tax";
other_cost('ELEC') = 20;
other_cost('NG') = 25;
* Electricity tax 7.03 EUR/MWh [http://vm.fi/energiaverotus]
* Transmission cost 13 EUR/MWh.
*   12.5 EUR from Helen data 2017 sample calculation:
*   https://www.helensahkoverkko.fi/uutiset/2017/siirtohinnat-muuttuvat/
*   2000 MWh/a, 2080,3 EUR/month transmission fee.

Table comm_prop(COMM, P_COMM) Commodity properties
                lhv    ex-c-f
    BIO         7.9      1.11 // NOTE factor for moist fuel LHV, at 20% moisture
    NG           50      1.04
    ELEC                    1
    SNG          50      1.04

     +     co2-df   co2-dr   co2-if
    BIO         0                 5
    NG        198
    ELEC                        640

     +         cp     temp   press      mmass
    BIO    1.3e-3   298.15
    NG     2.3e-3   298.15     5e6    16.5e-3
    SNG    2.3e-3   298.15     5e6    16.5e-3
;
* ## Exergy factors [Ertesvag2000]
* ## CO2 emissions:
* - Biomass: 4..7 kg/MWh [Wihersaari2005]
* - Methane: 2.75 kg / kg CH4 -> 198 kg / MWh CH4
* - Elec: marginal CO2 emission in Finnish system, exports considered,
*   current policy initiative 2030 [olkkonen2016spatial]

Table fg_prop(UNITS, FUEL, P_FG) Flue gas properties
                                  temp     f-fg       ex-c     cp   press
    (GE1 * GE4, GHOB1).NG       373.15    12.84     0.1095   1e-3     1e5
    (BIOCHP1, BHOB1).BIO        373.15    11.59    0.23114   1e-3     1e5
;

* Engine properties in CHP generation [wartsila2010chp]
Table GE_prop(P_UNIT, GE_TYPE) Gas engine properties
                   20V34SG    16V34SG    9L34SG
    eff-net-max      0.902      0.902     0.902
    eff-el-max       0.465      0.464     0.461
    max-el           8.730      6.970     3.888
;

* Coefficient of performance [energistyrelsen2012data]
Table COP(HEATPUMP, PERIODS)  Average heat pump coefficient of performance
             P1     P2     P3     P4     P5     P6     P7
HP1         2.8    2.8    2.8    2.8    2.8    2.8    2.8
;

* Efficiencies from [energistyrelsen2012data]
Table unit_prop(UNITS, P_UNIT) Unit properties
                eff-net-max    eff-el-max
    BHOB1              1.08
    GHOB1              1.01
    BIOCHP1            1.03          0.25
;

Parameter max_output(UNIT_FX, OUTP)  "Maximum output, fixed size units [MW]";

* Gas engines from GE_prop
max_output(GE, 'HDH') = INF;
max_output(GE, 'ELEC')
    = sum(GE_TYPE$map_GE(GE, GE_TYPE), GE_PROP('max-el', GE_TYPE));

Scalars
    T0               Environment reference temperature [K]   / 298.15 /
    p0               Environment reference pressure [Pa]     / 1e5 /
    R                "Gas constant [MJ/K/mol]"               / 8.314e-6 /
    c_CO2            Carbon dioxide emission cost [EUR*kg-1] / 0.015 /

    ann           Annuity factor / 0.09634 / // 15 years, 5 % interest rate

*   BioSNG production data from Kohl et al. (2014), fig. 2
*   doi:10.1016/j.energy.2014.03.107
    BSNG_el_f     BioSNG specific elec. consumption [W*(W_th-out)-1]
            / 0.1418 /
    BSNG_input_f  BioSNG specific biomass consumption [W*(W_th-out)-1]
            / 8.191 /
    BSNG_prod_f   BioSNG specific production [W*(W_th-out)-1]
            / 5.674 /
;


Positive variables

*   Units
    P_in(FEED, UNITS, PERIODS)   Process input power [MW (LHV)]
    P_out(OUTP, UNITS, PERIODS)  Process output power power [MW (LHV)]
    P_DH(UNITS, PERIODS)         Net heat to district heating grids [MW]
    m_CO2(UNITS, p_CO2, PERIODS) "CO2 flow due to feed use [kg/period]"
    m_in(FUEL, UNITS, PERIODS)      Product input flow [kg*s-1]
    sc_unit_size_dh(UNIT_SC)     "Scalable unit size: rated (maximum) DH power [MW]"

*   Costs
    inv_cost_sc(UNITS)         Scalable investment cost [MEUR]
    cost(UNITS, MONEY_ITEMS)   "Annual cost [MEUR/a]"
    income(UNITS, MONEY_ITEMS) "Annual income [MEUR/a]"

*   Exergy
    ex_feed(FEED, UNITS, PERIODS)       Feed exergy [MW]
    ex_fluegas(UNITS, PERIODS)          Flue gas exergy [MW]
    ex_outp(OUTP, UNITS, PERIODS)       Output flow exergy [MW]
    ex_loss(UNITS, PERIODS)             Unit exergy loss [MW]

*   Annual numbers
    energy_in(UNITS, FEED)    "Total feed consumption [MWh/a]"
    energy_out(UNITS, OUTP)   "Total electricity production [MWh/a]"
    m_fuel(UNITS, FUEL)       "Total fuel consumption [t/a]"
    mt_CO2(UNITS, p_CO2)      "Total CO2 flow from units [t/a]"
;

Binary variables
    z_units(UNITS)    Unit existence
    z_unit_on(UNITS, PERIODS)  Unit is in use during the period
;

Variables
    pri_e(UNITS, PERIODS)  Primary energy consumption with power bonus [MW]
    pri_ex(UNITS, PERIODS) Primary exergy consumption with power bonus [MW]

    ex_loss_t   "Total exergy losses [GWh/a]"
    profit      "Annual profit [MEUR/a]"
    fossil_CO2  "System-level fossil CO2 emissions [10^6 kg/a]"
    pexa_dh_sys System-level DH PeXa factor
    pef_sys     System-level DH primary energy factor
    system_cost Total system cost consisting of the previous
;

Equations

*   DH grids
    DH_supply(PERIODS)          Heat supply-demand match over periods
    DH_net_prod(UNITS, PERIODS) Net DH production from unit

*   Units
    Fuelheat_GE(GE, GE_TYPE, PERIODS)  Gas engine fuel need
    DH_load_GE(GE, GE_TYPE, PERIODS)   high temp DH provided by GE
    Order_ge(GE, GE_TYPE)     Build engines of the same type in set GE order

    EL_BSNG(BIOSNG, PERIODS)  Electricity need of BIOSNG unit
    Q1_BSNG(BIOSNG, PERIODS)  Moist biomass need of BIOSNG unit
    Q2_BSNG(BIOSNG, PERIODS)  Final product of BIOSNG unit

    EL_BCHP(BIOCHP, PERIODS)  Electricity produced in BIOCHP
    Q1_BCHP(BIOCHP, PERIODS)  Moist biomass need of BIOCHP unit

    EL_HP(HEATPUMP, PERIODS)  Heat pump electricity requirement

    Q1_BHOB(BIOHOB, PERIODS)  Moist biomass need of BIOHOB unit
    Q1_GHOB(GASHOB, PERIODS)  Natural gas need of GASHOB unit

    CO2_flow(UNITS, p_CO2, PERIODS)      CO2 flow from units
    Input_mass(UNITS, FUEL, PERIODS)     Input mass flows
    Output_limit(UNIT_FX, OUTP, PERIODS) Maximum output flow [MW]
    Output_limit_sc(UNIT_SC, PERIODS)    Maximum DH output [MW]
    Semicont_load_min(UNIT_FX, OUTP, PERIODS)  Semicontinuous load - minimum
    Semicont_load_max(UNIT_FX, OUTP, PERIODS)  Semicontinuous load - maximum
    Semicont_load_min_sc(UNIT_SC, PERIODS)     Semicontinuous DH load - minimum. Scalable units.
    Semicont_load_max_sc(UNIT_SC, PERIODS)     Semicontinuous DH load - maximum. Scalable units.
    Equip_exist(UNITS, PERIODS)          Unit existence
    Equip_exist_sc(UNIT_SC)              Unit existence - scalable units
    Equip_only_necessary(UNITS)          Build only plants that are used
    Investment_sc(UNITS)                 Investment cost for scalable units

*   Costs
    Cost_feed(UNITS, FEED)    Annual feed cost
    Cost_CO2(UNITS)           Annual CO2 cost
    Cost_inv(UNITS)           Annualized investment cost
    Income_outp(UNITS, OUTP)  Annual income

*   Exergy
    Exe_DH(UNITS, PERIODS)          DH exergy
    Exe_feed(UNITS, FEED, PERIODS)  Feed exergy
    Exe_fluegas(UNITS, PERIODS)     Flue gas exergy
    Exe_outp(UNITS, OUTP, PERIODS)  Output flow exergy
    Exe_loss(UNITS, PERIODS)        Exergy loss

*   Annual numbers
    E_out(UNITS, OUTP)              Annual energy output
    E_in(UNITS, FEED)               Annual energy input
    Mass_in(UNITS, FUEL)            Annual fuel input
    Mass_CO2(UNITS, p_CO2)          Annual CO2 output

    Primary_energy(UNITS, PERIODS)  Primary energy consumption (power bonus)
    Primary_exergy(UNITS, PERIODS)  Primary exergy consumption (power bonus)

*   Objective functions
    Obj_ex    Minimization of exergy losses
    Obj_prof  Maximimation of annual profit
    Obj_carb  Minimization of fossil CO2 emissions
    Obj_pexa  Minimization of system DH PeXa factor
    Obj_pef   Minimization of primary energy consumption
    Obj       Objective function
;



*------------------------------------------------------------------------
* Assignments

bigM(UNITS) = smax(PERIODS, heat_demand(PERIODS)) * 2;




*------------------------------------------------------------------------
* District heating grids

DH_supply(PERIODS)..
    sum(UNITS, P_out('HDH', UNITS, PERIODS))
    =E= heat_demand(PERIODS);

DH_net_prod(UNITS, PERIODS)..
    P_DH(UNITS, PERIODS) =E= P_out('HDH', UNITS, PERIODS);


*------------------------------------------------------------------------
* Units

* Gas engines

Fuelheat_GE(GE, GE_TYPE, PERIODS)$map_GE(GE, GE_TYPE)..
    GE_prop('eff-el-max', GE_TYPE) * P_in('NG', GE, PERIODS)
    =E= P_out('ELEC', GE, PERIODS) ;

DH_load_GE(GE, GE_TYPE, PERIODS)$map_GE(GE, GE_TYPE)..
    P_DH(GE, PERIODS)
    =E= GE_prop('eff-net-max', GE_TYPE) * P_in('NG', GE, PERIODS)
    - P_out('ELEC', GE, PERIODS);

* Only build next engine of the same type if previous has been built
Order_ge(GE, GE_TYPE)$(map_GE(GE, GE_TYPE) and map_GE(GE + 1, GE_TYPE))..
    z_units(GE + 1) =L= z_units(GE);


* BIOSNG

EL_BSNG(BIOSNG, PERIODS)..
    P_in('ELEC', BIOSNG, PERIODS) =E= BSNG_el_f * P_DH(BIOSNG, PERIODS);

Q1_BSNG(BIOSNG, PERIODS)..
    P_in('BIO', BIOSNG, PERIODS) =E= BSNG_input_f * P_DH(BIOSNG, PERIODS);

Q2_BSNG(BIOSNG, PERIODS)..
    P_out('SNG', BIOSNG, PERIODS) =E= BSNG_prod_f * P_DH(BIOSNG, PERIODS);


* BIOCHP

EL_BCHP(BIOCHP, PERIODS)..
    P_out('ELEC', BIOCHP, PERIODS)
    =E= unit_prop(BIOCHP, 'eff-el-max') * P_in('BIO', BIOCHP, PERIODS);

Q1_BCHP(BIOCHP, PERIODS)..
    P_in('BIO', BIOCHP, PERIODS) * unit_prop(BIOCHP, 'eff-net-max')
    =E= P_DH(BIOCHP, PERIODS) + P_out('ELEC', BIOCHP, PERIODS);


* Heat pumps

EL_HP(HEATPUMP, PERIODS)..
    P_in('ELEC', HEATPUMP, PERIODS) * COP(HEATPUMP, PERIODS)
    =E= P_DH(HEATPUMP, PERIODS);


* BIOHOB

Q1_BHOB(BIOHOB, PERIODS)..
    P_in('BIO', BIOHOB, PERIODS) * unit_prop(BIOHOB, 'eff-net-max')
    =E= P_DH(BIOHOB, PERIODS);

* GASHOB

Q1_GHOB(GASHOB, PERIODS)..
    P_in('NG', GASHOB, PERIODS) * unit_prop(GASHOB, 'eff-net-max')
    =E= P_DH(GASHOB, PERIODS);


* Limits

CO2_flow(UNITS, p_CO2, PERIODS)..
    m_CO2(UNITS, p_CO2, PERIODS)
    =E= sum(FEED$map_in(UNITS, FEED),
        P_in(FEED, UNITS, PERIODS) * time(PERIODS)
        * comm_prop(FEED, p_CO2));

Input_mass(UNITS, FUEL, PERIODS)$map_in(UNITS, FUEL)..
    m_in(FUEL, UNITS, PERIODS) * comm_prop(FUEL, 'lhv')
    =E= P_in(FUEL, UNITS, PERIODS);

Output_limit(UNIT_FX, OUTP, PERIODS)$map_out(UNIT_FX, OUTP)..
    P_out(OUTP, UNIT_FX, PERIODS) =L= max_output(UNIT_FX, OUTP);

Output_limit_sc(UNIT_SC, PERIODS)..
    P_DH(UNIT_SC, PERIODS) =L= sc_unit_size_dh(UNIT_SC);

Semicont_load_min(UNIT_FX, OUTP, PERIODS)$min_load(UNIT_FX, OUTP)..
    P_out(OUTP, UNIT_FX, PERIODS)
    =G= min_load(UNIT_FX, OUTP) * max_output(UNIT_FX, OUTP)
    * z_unit_on(UNIT_FX, PERIODS);

Semicont_load_max(UNIT_FX, OUTP, PERIODS)$min_load(UNIT_FX, OUTP)..
    P_out(OUTP, UNIT_FX, PERIODS)
    =L= max_output(UNIT_FX, OUTP) * z_unit_on(UNIT_FX, PERIODS);

Semicont_load_min_sc(UNIT_SC, PERIODS)$min_load(UNIT_SC, 'HDH')..
    P_DH(UNIT_SC, PERIODS)
    =G= min_load(UNIT_SC, 'HDH') * sc_unit_size_dh(UNIT_SC) - bigM(UNIT_SC) * (1 - z_unit_on(UNIT_SC, PERIODS));

Semicont_load_max_sc(UNIT_SC, PERIODS)$min_load(UNIT_SC, 'HDH')..
    P_DH(UNIT_SC, PERIODS)
    =L= bigM(UNIT_SC) * z_unit_on(UNIT_SC, PERIODS);

* Unit existence

Equip_exist(UNITS, PERIODS)..
    P_DH(UNITS, PERIODS) =L= bigM(UNITS) * z_units(UNITS);

Equip_exist_sc(UNIT_SC)..
    sc_unit_size_dh(UNIT_SC)
    =l= bigM(UNIT_SC) * z_units(UNIT_SC);

Equip_only_necessary(UNITS)..
    z_units(UNITS) =l= sum(PERIODS, P_DH(UNITS, PERIODS));

Investment_sc(UNIT_SC)..
    inv_cost_sc(UNIT_SC)
    =E= sc_unit_size_dh(UNIT_SC) * s_inv_DH(UNIT_SC);



*------------------------------------------------------------------------
* Costs

Cost_feed(UNITS, FEED)$map_in(UNITS, FEED)..
    cost(UNITS, FEED)
    =E= sum(PERIODS,
        time(PERIODS) * P_in(FEED, UNITS, PERIODS)
         * (price(FEED, 'buy') + other_cost(FEED))
    ) * 1e-6 ;

Cost_CO2(UNITS)..
    cost(UNITS, 'CO2')
    =E= sum(PERIODS, c_CO2 * m_CO2(UNITS, 'co2-df', PERIODS)) * 1e-6 ;

Cost_inv(UNITS)..
    cost(UNITS, 'INV')
    =E= ann * (inv_cost_fx(UNITS)$UNIT_FX(UNITS) * z_units(UNITS)
        + inv_cost_sc(UNITS)$UNIT_SC(UNITS));

Income_outp(UNITS, OUTP)$map_out(UNITS, OUTP)..
    income(UNITS, OUTP)
    =E= sum(PERIODS,
        time(PERIODS) * price(OUTP, 'sell') * P_out(OUTP, UNITS, PERIODS)
    ) * 1e-6 ;


*TODO muut käyttökustannukset?



*------------------------------------------------------------------------
* Exergy
*
* For ideal fluid, assuming constant cp and excluding pressure variation:
*     E = cp * m * ((T - T0) - T0 * log(T / T0))
*     E * (Tout - Tin) = Q * ((T - T0) - T0 * log(T / T0))

* Exergy difference between DH input and output
* B_{DH} = B_{DH, out} - B_{DH, in}
* NOTE If you ever change this, also go fix Obj_pexa
Exe_DH(UNITS, PERIODS)$map_out(UNITS, 'HDH')..
    ex_outp('HDH', UNITS, PERIODS)
    * (t_out(PERIODS) - t_in(PERIODS))
    =E= P_out('HDH', UNITS, PERIODS) * (
        (t_out(PERIODS) - t_in(PERIODS))
        - T0 * log(t_out(PERIODS) / t_in(PERIODS))
    );

* Feed exergy flow
* Includes fuel flows and electricity
Exe_feed(UNITS, FEED, PERIODS)$map_in(UNITS, FEED)..
    ex_feed(FEED, UNITS, PERIODS)
    =E= P_in(FEED, UNITS, PERIODS) * comm_prop(FEED, 'ex-c-f') // ex_c
    + (P_in(FEED, UNITS, PERIODS) / comm_prop(FEED, 'lhv'))$(
        comm_prop(FEED, 'lhv')) * (
            comm_prop(FEED, 'cp') * (
                (comm_prop(FEED, 'temp') - T0)
                - T0 * log(comm_prop(FEED, 'temp') / T0)
            )$comm_prop(FEED, 'temp')
            + (R / comm_prop(FEED, 'mmass') * T0 * log(
                comm_prop(FEED, 'press') / p0)
            )$GAS(FEED)
        )                                                      // ex_ph
    ;

Exe_fluegas(UNITS, PERIODS)..
    ex_fluegas(UNITS, PERIODS)
    =E= sum(FUEL$(map_in(UNITS, FUEL) and fg_prop(UNITS, FUEL, 'f-fg')),
        fg_prop(UNITS, FUEL, 'f-fg') *  m_in(FUEL, UNITS, PERIODS) * (
            fg_prop(UNITS, FUEL, 'cp') * (
                (fg_prop(UNITS, FUEL, 'temp') - T0)
                - T0 * log(fg_prop(UNITS, FUEL, 'temp') / T0)
            )$fg_prop(UNITS, FUEL, 'temp')
            + (R / fg_prop(UNITS, FUEL, 'mmass') * T0 * log(
                fg_prop(UNITS, FUEL, 'press') / p0)
            )$fg_prop(UNITS, FUEL, 'mmass')
            + fg_prop(UNITS, FUEL, 'ex-c')
        )
    );

* Output flow exergy content
* Note: heat not included (calculated in Exe_DH)
Exe_outp(UNITS, OUTP, PERIODS)$(map_out(UNITS, OUTP) and not DHGRID(OUTP))..
    ex_outp(OUTP, UNITS, PERIODS)
    =E= P_out(OUTP, UNITS, PERIODS) * comm_prop(OUTP, 'ex-c-f') // ex_c
    + (P_out(OUTP, UNITS, PERIODS) / comm_prop(OUTP, 'lhv'))$(
        comm_prop(OUTP, 'lhv')) * (
            comm_prop(OUTP, 'cp') * (
                (comm_prop(OUTP, 'temp') - T0)
                - T0 * log(comm_prop(OUTP, 'temp') / T0)
            )$comm_prop(OUTP, 'temp')
            + (R / comm_prop(OUTP, 'mmass') * T0 * log(
                comm_prop(OUTP, 'press') / p0)
            )$(GAS(OUTP) and comm_prop(OUTP, 'mmass'))
        )                                                       // ex_ph
    ;

Exe_loss(UNITS, PERIODS)..
    ex_loss(UNITS, PERIODS)
    =E= sum(FEED$map_in(UNITS, FEED), ex_feed(FEED, UNITS, PERIODS))
    - sum(OUTP$map_out(UNITS, OUTP), ex_outp(OUTP, UNITS, PERIODS))
    - ex_fluegas(UNITS, PERIODS) ;


* Annual outputs
E_out(UNITS, OUTP)$map_out(UNITS, OUTP)..
    energy_out(UNITS, OUTP)
    =E= sum(PERIODS, time(PERIODS) * P_out(OUTP, UNITS, PERIODS));

E_in(UNITS, FEED)$map_in(UNITS, FEED)..
    energy_in(UNITS, FEED)
    =E= sum(PERIODS, time(PERIODS) * P_in(FEED, UNITS, PERIODS));

Mass_in(UNITS, FUEL)$map_in(UNITS, FUEL)..
    m_fuel(UNITS, FUEL)
    =E= sum(PERIODS,
        time(PERIODS) * m_in(FUEL, UNITS, PERIODS) * 3600) * 1e-3;

Mass_CO2(UNITS, p_CO2)..
    mt_CO2(UNITS, p_CO2)
    =E= sum(PERIODS, m_CO2(UNITS, p_CO2, PERIODS)) * 1e-3;


* Primary energy consumption is calculated with power bonus method
Primary_energy(UNITS, PERIODS)..
    pri_e(UNITS, PERIODS)
    =E= (sum(FEED$map_in(UNITS, FEED), P_in(FEED, UNITS, PERIODS) * pef(FEED))
        - sum(OUTP$(map_out(UNITS, OUTP) and not DHGRID(OUTP)),
            P_out(OUTP, UNITS, PERIODS) * pef(OUTP)));

Primary_exergy(UNITS, PERIODS)..
    pri_ex(UNITS, PERIODS)
    =E= sum(FEED$map_in(UNITS, FEED),
        ex_feed(FEED, UNITS, PERIODS) * pef(FEED))
    - sum(OUTP$(map_out(UNITS, OUTP) and not DHGRID(OUTP)),
        ex_outp(OUTP, UNITS, PERIODS) * pef(OUTP));



*------------------------------------------------------------------------
* Objective functions

Obj_ex..
    ex_loss_t =E= sum((UNITS, PERIODS),
        ex_loss(UNITS, PERIODS) * time(PERIODS)) * 1e-3;

Obj_prof..
    profit
    =E= sum((UNITS, OUTP)$map_out(UNITS, OUTP), income(UNITS, OUTP))
    - sum((UNITS, FEED)$map_in(UNITS, FEED), cost(UNITS, FEED))
    - sum(UNITS, cost(UNITS, 'CO2'))
    - sum(UNITS, cost(UNITS, 'INV'));

Obj_carb..
    fossil_CO2 =E= sum((UNITS, FEED, p_CO2_f, PERIODS)$map_in(UNITS, FEED),
        // Direct/indirect fossil CO2 output from feed utilization,
        // including indirect emissions from biomass/imported electricity
        m_CO2(UNITS, p_CO2_f, PERIODS)
        // System level CO2 reduction
        - P_out('ELEC', UNITS, PERIODS)$(map_in(UNITS, 'BIO')
            and map_out(UNITS, 'ELEC'))
        * time(PERIODS) * comm_prop('ELEC', 'co2-if')
    ) / 1E6 ;

* NOTE District heat exergy flow can be calculated from the initial parameters.
* Therefore also simplifying former summation in
* Obj_pexa..
*     pexa_dh_sys * sum((UNITS, PERIODS)$map_out(UNITS, 'HDH'),
*                       ex_outp('HDH', UNITS, PERIODS) * time(PERIODS))
*     =E= sum((UNITS, PERIODS), pri_ex(UNITS, PERIODS) * time(PERIODS));
* to
*     sum(PERIODS,
*         heat_demand(PERIODS) / (t_out(PERIODS) - t_in(PERIODS))
*         * ( (t_out(PERIODS) - t_in(PERIODS))
*             - T0 * log(t_out(PERIODS) / t_in(PERIODS))) 
*         * time(PERIODS))
* and there is no need to worry about division by zero.
Obj_pexa..
    pexa_dh_sys * sum(PERIODS,
        heat_demand(PERIODS) / (t_out(PERIODS) - t_in(PERIODS))
        * ( (t_out(PERIODS) - t_in(PERIODS))
            - T0 * log(t_out(PERIODS) / t_in(PERIODS)))
        * time(PERIODS))
    =E= sum((UNITS, PERIODS), pri_ex(UNITS, PERIODS) * time(PERIODS));

* NOTE Denominator is total energy amount, which is trivial to calculate.
* Thus we simplify objective function: in
*     Obj_pef..
*         pef_sys * sum((UNITS, PERIODS)$map_out(UNITS, 'HDH'),
*                       P_out('HDH', UNITS, PERIODS) * time(PERIODS))
*         =E= sum((UNITS, PERIODS), pri_e(UNITS, PERIODS) * time(PERIODS));
* we may replace the former summation with
*     sum(PERIODS, heat_demand(PERIODS) * time(PERIODS))
* and there is no need to worry about division by zero.
Obj_pef..
    pef_sys * sum(PERIODS, heat_demand(PERIODS) * time(PERIODS))
    =E= sum((UNITS, PERIODS), pri_e(UNITS, PERIODS) * time(PERIODS));

Obj..
    system_cost =E= -wt('C1') * profit + wt('C2') * ex_loss_t
    + wt('C3') * fossil_CO2 + wt('C4') * pef_sys
    + wt('C5') * pexa_dh_sys ;



*------------------------------------------------------------------------
* Limitations, initial values and options for the solver

P_out.UP('ELEC', GE, PERIODS) = max_output(GE, 'ELEC');
*P_out.LO('ELEC', GE, PERIODS) = 0.0000003 * max_output(GE, 'ELEC');

* Give initial values to all scalable units
sc_unit_size_dh.l(UNIT_SC) = smax(PERIODS, heat_demand(PERIODS)) * 0.3;


 Option optcr    = 0 ;
 Option limrow   = 10 ;
 Option limcol   = 10 ;
 Option solprint = on ;
 Option sysout   = on ;
 Option iterlim  = 1000000000 ;
 Option reslim   = 1000000000 ;
Option MIP=CPLEX;


MODEL REGION1 / ALL / ;

SOLVE REGION1 using MIP minimizing system_cost;



*------------------------------------------------------------------------
* Calculate display values from model output

Parameters
    load(UNITS, OUTP, PERIODS)      Unit loading rate (0..1)
    pef_DH(UNITS, PERIODS)    DH primary energy factor
    pee_DH(UNITS, PERIODS)    DH primary energy efficiency
    ex_eff(UNITS, PERIODS)    "Unit exergy efficiency [0..1]"
    pexa_DH(UNITS, PERIODS)   DH primary exergy factor
    lcoe                      "Levelized cost of energy [EUR/MWh]"
    dhp_pb                    "DH price (power bonus method) [EUR/MWh]"
;

load(UNIT_FX, OUTP, PERIODS)
    = (P_out.l(OUTP, UNIT_FX, PERIODS) / max_output(UNIT_FX, OUTP)
    )$max_output(UNIT_FX, OUTP);
load(UNIT_SC, 'HDH', PERIODS)
    = (P_DH.l(UNIT_SC, PERIODS) / sc_unit_size_dh.l(UNIT_SC)
    )$(map_out(UNIT_SC, 'HDH') and sc_unit_size_dh.l(UNIT_SC));

* Primary energy efficiency is calculated with power bonus method
pef_DH(UNITS, PERIODS)$(z_units.l(UNITS) and P_DH.l(UNITS, PERIODS)) =
    pri_e.l(UNITS, PERIODS) / P_DH.l(UNITS, PERIODS);
pee_DH(UNITS, PERIODS)$pef_DH(UNITS, PERIODS) = 1 / pef_DH(UNITS, PERIODS);

ex_eff(UNITS, PERIODS)$(sum(FEED, ex_feed.l(FEED, UNITS, PERIODS)) gt 0)
    = (sum(FEED, ex_feed.l(FEED, UNITS, PERIODS)) - Ex_loss.l(UNITS, PERIODS))
    / sum(FEED, ex_feed.l(FEED, UNITS, PERIODS)) ;

* Primary exergy factor for district heat [laukkanen_primary_2015, eq. 1, 25]
* Using "power bonus" method, electricity (and other product) generation
* reduces DH primary exergy value
pexa_DH(UNITS, PERIODS)$(z_units.l(UNITS)
        and ex_outp.l('HDH', UNITS, PERIODS))
    = pri_ex.l(UNITS, PERIODS)
    / ex_outp.l('HDH', UNITS, PERIODS);

* Prices
lcoe = (sum((UNITS, MONEY_ITEMS), cost.l(UNITS, MONEY_ITEMS)) * 1e6 / sum(
    (UNITS, OUTP), energy_out.l(UNITS, OUTP)))$(sum((UNITS, OUTP), energy_out.l(UNITS, OUTP)) gt 0);
dhp_pb = ((sum((UNITS, MONEY_ITEMS), cost.l(UNITS, MONEY_ITEMS))
    - sum((UNITS, OUTP)$(not DHGRID(OUTP)),
        income.l(UNITS, OUTP))) * 1e6 / sum(
    UNITS, energy_out.l(UNITS, 'HDH')))$(sum(UNITS, energy_out.l(UNITS, 'HDH')) gt 0);

Option P_in:2:1:1;
Option P_out:2:1:1;

Display
    inv_cost_fx, inv_cost_sc.l,
    cost.l, income.l,
    ex_feed.l, ex_fluegas.l,
    ex_outp.l, ex_loss.l,
    m_in.l, m_CO2.l
    P_in.l, P_out.l
    sc_unit_size_dh.l
    load
    energy_in.l
    energy_out.l
    m_fuel.l
    mt_CO2.l
    pri_e.l, pri_ex.l
    pef_DH, pee_DH, ex_eff, pexa_DH
    lcoe, dhp_pb
;


execute_unload "Regional.gdx";
