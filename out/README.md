This folder houses output files from processing scripts.

Keep this empty folder in git, so that the output scripts do not have to check for existence.
